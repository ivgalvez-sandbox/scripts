#!/bin/sh

# TODO Document.
#=============================================================================
# Copyright 2014 Ivan Galvez Junquera (ivgalvez@gmail.com).
#
# Distributed under the OSI-approved BSD "July 22 1999" style license;
# Complete text of the original BSD "July 22 1999" license can be found in
# /usr/share/common-licenses/BSD on Debian systems.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================

for i in * ; do [ -f $i ] && mv -i $i `echo $i | uconv -x any-name | perl -wpne 's/ WITH [^}]+//g;' | uconv -x name-any | uconv -x any-latin -t iso-8859-1 -c | uconv -f iso-8859-1 -t ascii -x latin-ascii`; done;
