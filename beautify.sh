#!/bin/sh

#=============================================================================
# This script looks for all source code files recursively from the current 
# directory, and executes the following commands on each file:
# - dos2unix: Converts filei format to Unix.  Requires package 'tofrodos'.
# - cb++:     Corrects inlining and beautifies the source code using 'bcpp'.
# - Deletes the backup and other unnecesary files.
#
# Only .h, .c and .cpp files are processed.
# Error messages (i.e. not having permissions to acces a file) are redirected 
# to /dev/null.
#
#=============================================================================
# Copyright 2010 Ivan Galvez Junquera (ivgalvez@gmail.com). 
# 
# Distributed under the OSI-approved BSD "July 22 1999" style license;
# Complete text of the original BSD "July 22 1999" license can be found in 
# /usr/share/common-licenses/BSD on Debian systems.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================

find . -follow -name "*.h" -o -name "*.cpp" -o -name "*.c" 2>/dev/null | xargs dos2unix
find . -follow -name "*.h" -o -name "*.cpp" -o -name "*.c" 2>/dev/null | xargs cb++
find . -follow -name "*.orig" 2>/dev/null | xargs rm
