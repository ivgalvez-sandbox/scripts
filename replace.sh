#!/bin/sh

# TODO Document.

#=============================================================================
# Copyright 2009 Ivan Galvez Junquera (ivgalvez@gmail.com).
#
# Distributed under the OSI-approved BSD "July 22 1999" style license;
# Complete text of the original BSD "July 22 1999" license can be found in
# /usr/share/common-licenses/BSD on Debian systems.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================

if [ $# -le 2 ]; then
	echo 'Usage: replace string1 string2 FILE'
	echo '       This script replaces string1 for string2 in FILE.'
	echo '       You should use "" for the strings if they contain spaces or other special characters.'
	echo '       You can use patterns like * for FILE parameter.'
	exit -1
fi
for var in "$@"
do
	if [ "$var" = "$1" ] || [ "$var" = "$2" ]; then
		echo ...skipping $var
		continue
	fi
	echo 'Replacing "'$1'" for "'$2'" in '$var
   sed -i -e 's/'"$1"'/'"$2"'/g' $var $var
done


