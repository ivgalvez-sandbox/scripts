#!/bin/sh

# TODO Document.
#=============================================================================
# Copyright 2010 Ivan Galvez Junquera (ivgalvez@gmail.com).
#
# Distributed under the OSI-approved BSD "July 22 1999" style license;
# Complete text of the original BSD "July 22 1999" license can be found in
# /usr/share/common-licenses/BSD on Debian systems.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================

for i in * ; do [ -f $i ] && mv -i $i `echo $i | tr ':upper:' ':lower:'`;done;
