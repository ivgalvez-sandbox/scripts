#!/bin/sh

# TODO Document.

#=============================================================================
# Copyright 2008 Ivan Galvez Junquera (ivgalvez@gmail.com).
#
# Distributed under the OSI-approved BSD "July 22 1999" style license;
# Complete text of the original BSD "July 22 1999" license can be found in
# /usr/share/common-licenses/BSD on Debian systems.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================

rm -rf *.vcproj* *.cmake CMakeCache.txt CMakeLists.txt.user *.sln *.ncb *.suo CMakeFiles/ *.dir Makefile *.make $1 2>/dev/null

for subs in $( grep -i add_subdirectory CMakeLists.txt | grep -v '^[ ]*#' | awk -F '(' '{print $2}' | awk -F ')' '{print $1}' )
do
   cd $subs
   $0
   cd -
done
