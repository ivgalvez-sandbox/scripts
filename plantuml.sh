#!/bin/sh
# Create *.png files from text @startuml code and output *.png images to ./doc/images folder
# Recusively search from current folder scanning *.c, *.cpp, *.h, and *.doc files
java  -Djava.awt.headless=true -jar /usr/share/plantuml/plantuml.jar -v -o $PWD/doc/images  "./**.(c|cpp|doc|h)"
doxygen
